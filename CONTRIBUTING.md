# Contributing

## Issues

If something is borked or something needs adding, please feel free to open an issue. If you feel that you can write / modify code so as to resolve this issue yourself, please feel free to do so. Please be aware that it may take some time for the issue to be resolved by volunteer contributors, and if it is out of scope or infeasible it may be closed without resolution.

## Minor Contributions

In order to make minor contributions to the repo, please fork the repo and submit a merge request with your contributions. If you feel that you will be making such minor contributions often consider requesting access to the repo and making said contributions in a branch.

## Regular / Major Contributions

If you wish to contribute significantly to this repo, either by making regular minor contributions or occasional major contributions, you may request access to this repo, and it will be granted at the discretion of the maintainers (i.e. if it seems like you're an okay person you'll be let in).

## Accepting / Rejecting of Merge Requests

If your merge request is good and in scope with the project it will be merged. If your merge request is bad but in scope, changes will be requested. If your making these changes results in a good and in scope merge request, it will be merged. If your merge request is garbage code or out of scope, it will be rejected. The definition of good, bad, garbage, and in scope are determined according to existing code quality, scope stated in comments, and the particular position of Venus in the sky. Unfortunately, it is not known how the position of Venus contributes to these determinations other than that it is known to be the strongest contributing factor.

Note: this is not meant to discourage contributions, and if you disagree with these guidelines please feel free to fork this repo and write whatever the hell you want. This is meant to reduce workload for me - I welcome good contributions, and am happy to spend some time helping people who are trying to make good contributions, but first and foremost I want to write a graphics library, so if your contribution is not conducive to this it may not be included.

## Licensing And Other Legal Stuff

When you make changes to this repo, your changes are assumed to belong to you but be licensed under the MIT licence, hence allowing this repo to remain MIT. There is currently no contributor license agreement or similar because frankly that's a pain and I can't be bothered. If you decide to be a copyright / licence troll with regards to this repo your contributions will be purged and your access revoked.

## Existing Contributors

The following people are currently contributing to the repo. Their specific code contributions may be seen by looking at the commit log, though note that they may have also contributed in other ways (e.g. designing the algorithms or optimisations that are later implemented by someone else).

- Kajetan Champlewski (mildlyincompetent)
- Dan Trickey (trickeydan)

