/*
    A113 - Integer Maths

    This file contains various useful functions to perform integer maths.
    This is useful because we do not have an FPU.
*/

#include <stdint.h>

// Calculate the square root of x using only 16-bit integer maths.
uint16_t a113_imath_sqrt16(uint16_t x);

