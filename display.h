/*
	A113 Display Driver
	-------------------

	This is the display driver for the A113 library. If you happen to be
	attempting a port of this library to, say, the Il Matto, you probably want
	to start here, as this is the part that talks most directly to the
	hardware. That said I'm told the Il Matto already has some pretty good
	graphics libraries so it may not be worth the hassle.

	If you're just here to use the graphics library, you probably don't need
	much if any of the functions here. Note that as A113 is not a display
	driver library but instead a graphics library, contributions to the display
	driver will only be accepted if they are needed to improve the
	functionality of A113 itself; improving the experience of users of the
	library is out of scope as they shouldn't be using A113 as a display driver
	to begin with.

	If you're looking to improve the A113 library then pay a lot of attention
	to these functions because using them correctly (i.e. efficiently) and/or
	optimising them is probably the easiest way to make things go fast.
*/

#include <stdint.h>

/*	We define the addresses for writing commands and data as volatile pointers.
	They need to be volatile as we need all the reads/writes to actually happen
	when we ask for them, as the display controller will consume or emit
	additional values only when values are written or read. If these aren't
	declared volatile, the compiler will optimise away half the read/write
	operations and everything will be borked.								  */
#define A113_DISP_CMD_ADDR	((uint8_t volatile *) 0x4000)
#define A113_DISP_DATA_ADDR	((uint8_t volatile *) 0x4100)

// Writes a one byte command to the display driver.
// See the data sheet for available commands.
inline __attribute__((always_inline)) void a113_display_write_cmd(uint8_t cmd)
{
	*A113_DISP_CMD_ADDR = cmd;
}

// Writes one byte of data to the display driver.
// We use 16 bit pixels, so this will not be used often.
inline __attribute__((always_inline)) void a113_display_write_data(uint8_t data)
{
	*A113_DISP_DATA_ADDR = data;
}

// Writes 16 bits of data to the display driver.
// This is generally the way we write a pixel.
inline __attribute__((always_inline)) void a113_display_write_data16(uint16_t data)
{
	*A113_DISP_DATA_ADDR = (uint8_t) (data >> 8);
	*A113_DISP_DATA_ADDR = (uint8_t) (data & 0x00FF);
}

// Reads 8 bits of data from the display driver.
inline __attribute__((always_inline)) uint8_t a113_display_read_data()
{
	return *A113_DISP_DATA_ADDR;
}

// Reads 16 bits of data from the display driver.
inline __attribute__((always_inline)) uint16_t a113_display_read_data16()
{
	uint16_t a, b;
	a = (uint16_t) *A113_DISP_DATA_ADDR << 8;
	b = (uint16_t) *A113_DISP_DATA_ADDR;
	return a+b;
}

// Macro to easily set the display x and y range.
#define A113_DISPLAY_SET_RANGE(x1, x2, y1, y2) do {\
	a113_display_write_cmd(0x2A);\
	a113_display_write_data16(x1);\
	a113_display_write_data16(x2);\
	a113_display_write_cmd(0x2B);\
	a113_display_write_data16(y1);\
	a113_display_write_data16(y2);\
	} while(0)

// Initialises the display driver, turns display on, etc.
// Use either this or another library's display init (should be compatible).
void a113_display_init();

