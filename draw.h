/*
	A113 Drawing Functionality
	--------------------------

	These functions draw various 2D shapes on the screen. This drawing is
	meant to be reasonably optimised, but further optimisations are welcomed,
	as are other useful contributions.
*/

#include <stdint.h>

typedef struct A113DisplayPoint {
	int16_t x;
	int16_t y;
} A113DisplayPoint;

typedef struct A113Rectangle {
	A113DisplayPoint a;
	A113DisplayPoint b;
} A113Rectangle;

typedef struct A113StraightLine {
	A113DisplayPoint a;
	A113DisplayPoint b;
} A113StraightLine;

typedef struct A113Circle {
	A113DisplayPoint c;
	int8_t r;
} A113Circle;

// Draws a rectangle from (x1, y1) to (x2, y2) and colours it in the given colour.
void a113_draw_rectangle_fill(A113Rectangle r, uint16_t colour);

// Draws a rectangle outline from (x1, y1) to (x2, y2) in the given colour.
void a113_draw_rectangle_outline(A113Rectangle r, uint16_t colour);

// Draws a continuous 1px wide line from (x1, y1) to (x2, y2) in the given colour.
void a113_draw_line(A113StraightLine l, uint16_t colour);

// Draw a single pixel at p, in the given colour
void a113_draw_point(A113DisplayPoint p, uint16_t colour);

// Draws a circle with centre c and radius r in the given colours
// This function will overwrite all pixels within the circle's bounding rectangle.
void a113_draw_circle(A113Circle c, uint16_t fg_col, uint16_t bg_col);

// Draws a circle with centre c and radius r in the given colour
void a113_draw_frame_circle(A113Circle c, uint16_t colour);
