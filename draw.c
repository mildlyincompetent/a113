#include "draw.h"
#include "display.h"
#include "imath.h"
#include <stdlib.h>

static inline void swap_if_larger(int16_t *a, int16_t *b)
{
	if(*a > *b){
		int16_t temp = *a;
		*a = *b;
		*b = temp;
	}
}

void a113_draw_rectangle_fill(A113Rectangle r, uint16_t colour)
{
	// Perform swaps to make sure things are correctly ordered.
	swap_if_larger(&r.a.x, &r.b.x);
	swap_if_larger(&r.a.y, &r.a.y);

	// Set display range
	A113_DISPLAY_SET_RANGE(r.a.x, r.b.x, r.a.y, r.b.y);

	// Fill content with colour.
	a113_display_write_cmd(0x2C);
	int16_t xr, yr, x, y;
	xr = r.b.x - r.a.x;
	yr = r.b.y - r.a.y;
	for(x = 0; x <= xr; x++){
		for(y = 0; y <= yr; y++){
			a113_display_write_data16(colour);
		}
	}
}

/*	TODO: Add an optimised drawing method for small rectangles, such as 5x5s,
	where bothering to do 4 separate screen draw operations is a huge waste of
	time. Such an optimised method could be implemented in this function
	although that would require annoying runtime checks, or as a separate
	function, although that would require programmers to know what they are
	doing. Clearly both have their positives and negatives, and the
	implementation detail shall be left to whoever can actually be bothered to
	do it.																	  */
void a113_draw_rectangle_outline(A113Rectangle r, uint16_t colour)
{
	// Usual swaps for correctness.
	swap_if_larger(&r.a.x, &r.b.x);
	swap_if_larger(&r.a.y, &r.b.y);

	// Determine ranges, define loop variable.
	int16_t xr, yr, i;
	xr = r.b.x - r.a.x;
	yr = r.b.y - r.a.y;

	// Draw top horizontal.
	A113_DISPLAY_SET_RANGE(r.a.x, r.b.x, r.a.y, r.a.y);
	a113_display_write_cmd(0x2C);
	for(i = 0; i <= xr; i++){
		a113_display_write_data16(colour);
	}

	// Draw botton horizontal.
	A113_DISPLAY_SET_RANGE(r.a.x, r.b.x, r.b.y, r.b.y);
	a113_display_write_cmd(0x2C);
	for(i = 0; i <= xr; i++){
		a113_display_write_data16(colour);
	}

	// Draw leftmost horizontal
	A113_DISPLAY_SET_RANGE(r.a.x, r.a.x, r.a.y, r.b.y);
	a113_display_write_cmd(0x2C);
	for(i = 0; i <= yr; i++){
		a113_display_write_data16(colour);
	}

	// Draw rightmost horizontal
	A113_DISPLAY_SET_RANGE(r.b.x, r.b.x, r.a.y, r.b.y);
	a113_display_write_cmd(0x2C);
	for(i = 0; i <= yr; i++){
		a113_display_write_data16(colour);
	}
}

void a113_draw_line(A113StraightLine l, uint16_t colour)
{
	// Special case: vertical line.
	if(l.a.x == l.b.x){
		A113_DISPLAY_SET_RANGE(l.a.x, l.a.x, l.a.y, l.b.y);
		int16_t ry, yv;
		ry = abs(l.b.y - l.a.y);
		a113_display_write_cmd(0x2C);
		for(yv = 0; yv <= ry; yv++){
			a113_display_write_data16(colour);
		}
		return;
	}

	// Special case: horizontal line.
	if(l.a.y == l.b.y){
		A113_DISPLAY_SET_RANGE(l.a.x, l.b.x, l.a.y, l.a.y);
		int16_t rx, xv;
		rx = abs(l.b.x - l.a.x);
		a113_display_write_cmd(0x2C);
		for(xv = 0; xv <= rx; xv++){
			a113_display_write_data16(colour);
		}
		return;
	}

	/*
		Attempting to draw diagonal lines quickly
		-----------------------------------------

		Here we utilise Bresenham's line algorithm (the integer mathematics
		version to compute the points we should plot on the line. However, the
		algorithm "plots" each point individually. This would be horrifically
		inefficient to implement 1 to 1, as plotting one pixel still requires us
		to address that pixel.

		We could simply store all the pixels and plot them later, except that
		means allocating space for up to 560 pixels. With each pixel being two
		coordinates and each coordinate being two bytes, we suddenly lose 2k of
		our memory. This is not very good.

		The alternative implemented here is to address the entire current
		vertical or horizontal segment, and only write as much as we need to
		before switching to the next segment.
	*/

	void plot_line_low(int16_t x0, int16_t x1, int16_t y0, int16_t y1)
	{
		int16_t dx = x1 - x0;
		int16_t dy = y1 - y0;
		int8_t yi = 1;
		if(dy < 0){
			yi = -1;
			dy = -dy;
		}
		int16_t dd = 2*dy - dx;
		int16_t y = y0;

		A113_DISPLAY_SET_RANGE(x0, 0xFFFF, y, y);
		a113_display_write_cmd(0x2C);
		for(int16_t x = x0; x <= x1; x++){
			a113_display_write_data16(colour);
			if(dd > 0){
				y += yi;
				dd -= 2*dx;
				A113_DISPLAY_SET_RANGE(x, 0xFFFF, y, y);
				a113_display_write_cmd(0x2C);
			}
			dd += 2*dy;
		}
	}

	void plot_line_high(int16_t x0, int16_t x1, int16_t y0, int16_t y1)
	{
		int16_t dx = x1 - x0;
		int16_t dy = y1 - y0;
		int8_t xi = 1;
		if(dx < 0){
			xi = -1;
			dx = -dx;
		}
		int16_t dd = 2*dx - dy;
		int16_t x = x0;

		A113_DISPLAY_SET_RANGE(x, x, y0, 0xFFFF);
		a113_display_write_cmd(0x2C);
		for(int16_t y = y0; y <= y1; y++){
			a113_display_write_data16(colour);
			if(dd > 0){
				x += xi;
				dd -= 2*dy;
				A113_DISPLAY_SET_RANGE(x, x, y, 0xFFFF);
				a113_display_write_cmd(0x2C);
			}
			dd += 2*dx;
		}
	}

	if(abs(l.b.y - l.a.y) < abs(l.b.x - l.a.x)){
		if(l.a.x > l.b.x){
			plot_line_low(l.b.x, l.a.x, l.b.y, l.a.y);
		} else {
			plot_line_low(l.a.x, l.b.x, l.a.y, l.b.y);
		}
	} else {
		if(l.a.y > l.b.y){
			plot_line_high(l.b.x, l.a.x, l.b.y, l.a.y);
		} else {
			plot_line_high(l.a.x, l.b.x, l.a.y, l.b.y);
		}
	}
}

void a113_draw_point(A113DisplayPoint p, uint16_t colour)
{
	// Set the pixel as the drawing area.
	A113_DISPLAY_SET_RANGE(p.x, p.x, p.y, p.y);

	//Draw the pixel
	a113_display_write_cmd(0x2C);
	a113_display_write_data16(colour);
}

void a113_draw_circle(A113Circle c, uint16_t fg_col, uint16_t bg_col)
{
	int16_t x1, y1, x2, y2, x, y;

	// Calculate bounding box of circle. (x1,y1), (x2,y2)
	x1 = c.c.x - c.r;
	x2 = c.c.x + c.r;
	y1 = c.c.y - c.r;
	y2 = c.c.y + c.r;

	// Set bounding box as drawing area.
	A113_DISPLAY_SET_RANGE(x1, x2, y1, y2);
	a113_display_write_cmd(0x2C);

	// Draw the circle. Including a square background colour.
	for(y = -c.r; y <= c.r; y++){
		for(x = -c.r; x <= c.r; x++){
			
			if( (x * x) + (y * y) <= (c.r * c.r) ){
				a113_display_write_data16(fg_col);
			} else {
				a113_display_write_data16(bg_col);
			}
			
		}
	}
}

void a113_draw_frame_circle(A113Circle c, uint16_t colour)
{
	int16_t x1, x, y;

	// Calculate bounding box of circle. (x1,y1), (x2,y2)
	x1 = c.c.x - c.r;

	for(y = -c.r; y <= 0; y++){
		// Set bounding box as drawing area.
		for(x = 0; x <= c.r; x++){
			x = a113_imath_sqrt16((y * y) - (c.r * c.r));
			A113_DISPLAY_SET_RANGE(x1 + x , 0xFFFF, c.c.y + y, c.c.y + y);
			a113_display_write_cmd(0x2C);
			a113_display_write_data16(colour);
		}
	}
}
