#include "display.h"
#include <avr/io.h>
#include <util/delay.h>

/*
	Copyright Notice:
	Some of the a113_display_init function is a derivate work based on Steve
	Gunn's lcd.c (later modified by Klaus-Peter Zauner). That work was licensed
	under the Creative Commons Attribution License, so attribution for authors
	is required wherever this work is reused or built upon.
*/
void a113_display_init()
{
	// TODO: Figure out what this line does.
	XMCRB = _BV(XMM2) | _BV(XMM1);
	// Writing SRE to 1 on XMCRA enables the external memory interface.
	XMCRA = _BV(SRE);

	// Set PORTC7 and PORTB4 to output. PORTC7 is RESET on the LCD, while
	// PORTB4 sets the backlight (BLC).
	DDRC |= _BV(7);
	DDRB |= _BV(4);
	_delay_ms(1);

	// Set PORTC7 to low for 20ms then back to high and wait a bit. This is
	// connected to RESET on the LCD, and so has the effect of resetting it.
	PORTC &= ~_BV(7);
    _delay_ms(20);
    PORTC |= _BV(7);
	_delay_ms(120);

	// Turn off the display.
	a113_display_write_cmd(0x28);

	// Turn off sleep mode.
	a113_display_write_cmd(0x11);

	// Wait 5ms as required after exiting sleep mode.
	_delay_ms(5);

	// Steve Gunn did lots of power related things here, incorrectly according
	// to the data sheet. Dunno why, works fine without them.

	// Set the interface control to its default settings if needed.
	// Commented out because they're _default_ so shouldn't (?) need changing.
	//a113_display_write_cmd(0xF6);
	//a113_display_write_data(0x01);
	//a113_display_write_data(0x00);
	//a113_display_write_data(0x00);

	// Set the pixel format to 16 bit colour.
	a113_display_write_cmd(0x3A);
	a113_display_write_data(0x55);

	// Set the display to be 320x240, with the top-left corner being 0, 0.
	// Also set the colours to go RGB rather than BGR.
	a113_display_write_cmd(0x36);
	a113_display_write_data(0xE8);

	// Clear the display:
	// X from 0 to 319 (inclusive).
	a113_display_write_cmd(0x2A);
	a113_display_write_data16(0);
	a113_display_write_data16(319);
	// Y from 0 to 239 (inclusive).
	a113_display_write_cmd(0x2B);
	a113_display_write_data16(0);
	a113_display_write_data16(239);
	// Write lots of zeroes.
	a113_display_write_cmd(0x2C);
	uint16_t x, y;
	for(x = 0; x < 320; x++){
		for(y = 0; y < 240; y++){
			a113_display_write_data16(0x0000);
		}
	}

	// Turn on the display and wait 50ms for it to populate with something
	// other than blinding white.
	a113_display_write_cmd(0x29);
	_delay_ms(50);

	// Turn on the backlight by setting PORTB4 to high.
	PORTB |= _BV(4);
}

