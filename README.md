# A113 La Fortuna Graphics Library

The A113 library is a "fast" graphics library (in so far as an AVR can do fast graphics) for the La Fortuna, a device used in Computer Systems II and other modules at the University of Southampton. The La Fortuna consists of an AT90USB1286 microcontroller and a 240x320 TFT display driven by an ILI9341. If you have a device with a similar configuration (e.g. the Il Matto board used by ELEC students) you may be able to port this library if sufficient pain and suffering is applied.

Note that A113 is a graphics library and not an LCD driver library. It contains functionality for drawing to the screen as well as initialising the display and sending commands/data to it, but if you require full control of the ILI9341 LCD driver, it's probably best to write your own code or use other libraries. The primary purpose of this library is to allow for relatively easy creation, transformation, and drawing of graphics on the La Fortuna.

The library is MIT licensed, however be aware that certain library files may derive or be copied from other FOSS software. Said files have appropriate copyright / licence notices.

General documentation for the library will be placed here in due course (i.e. as elements are developed and so there actually is something to document).

