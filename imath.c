#include "imath.h"

uint16_t a113_imath_sqrt16(uint16_t x){
    register uint16_t // OR register uint16 OR register uint8 - respectively  
        root, remainder, place;  
  
    root = 0;  
    remainder = x;  
    place = 0x4000; //OR place = 0x4000; OR place = 0x40; - respectively  
  
    while (place > remainder)  
        place = place >> 2;  
    while (place)  
    {  
        if (remainder >= root + place)  
        {  
            remainder = remainder - root - place;  
            root = root + (place << 1);  
        }  
        root = root >> 1;  
        place = place >> 2;  
    }  
    return root; 
}